#!/bin/bash

set -e

for CERTIFICATE in NMI*.pdf; do
  RENAMED=$(echo ${CERTIFICATE%.*}  | cut -d'_' -f3)
  if [[ -n "$RENAMED" ]]; then
    echo ""INFO: Already renamed $CERTIFICATE for $RENAMED""
    continue
  fi

  PRODUCT=$(pdftotext -raw -f 1 -l 1 $CERTIFICATE - | grep "Project name" | awk -F':' '{print $2}' | sed -e 's/^[ \t]*//')
  echo "INFO: Renaming $CERTIFICATE for $PRODUCT"
  mv $CERTIFICATE "${CERTIFICATE%.*}_${PRODUCT}.pdf"
done
